import fs from 'fs';
import path from 'path';

const verbose = false;
function readDir(directory, object) {
  if (verbose) {
    console.log('Directory: ', directory);
  }
  if (fs.lstatSync(directory).isDirectory()) {
    const files = fs.readdirSync(directory);
    files.forEach((file) => {
      readDir(path.join(directory, file), object);
    });
  } else {
    const resourceName = getResourceName(directory);
    object[resourceName] = directory.split('\\').join('/');
  }
}

function getResourceName(directory) {
  let resourceName = path.parse(directory).name;
  if (directory.indexOf('Items') > -1) {
    resourceName = `item_${resourceName}`;
  }
  if (verbose) {
    console.log('Directory: ', resourceName);
  }
  return resourceName;
}

export function assetsToJson() {
  const assets = {};
  console.log('Generating json from assets folder');
  readDir('assets', assets);
  fs.writeFileSync('resources.json', JSON.stringify(assets, null, 2));
  console.log('Done with assets folder');
}

const assets = assetsToJson();
export default assets;
