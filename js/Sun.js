import Phaser from 'phaser';

import config from '../game.json';

export default class Sun extends Phaser.GameObjects.Sprite {
  constructor(scene) {
    super(scene, 0, 0, 'sun');
    this.setOrigin(0.5, 0.5);
    this.width = config.tile.width;
    this.height = config.tile.height;
    scene.add.existing(this);
    // todo calculate speed depending on time clock
    this.speed = 1000;
    this.rotateAngle = 0.0001 * this.speed;
    this.currentAngle = 90;
    this.rotateRadius = (config.width / 2) * 0.9;
  }

  update() {
    this.rotate();
  }

  rotate() {
    this.currentAngle += this.rotateAngle;
    const x = config.width / 2 + Math.cos(this.currentAngle) * this.rotateRadius;
    const y = 140 + Math.sin(this.currentAngle) * this.rotateRadius;

    this.x = x;
    this.y = y;
  }
}
