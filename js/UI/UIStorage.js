import config from '../../game.json';
import UIWindow from './UIWindow';

export default class UIStorage extends UIWindow {
  constructor(scene, array) {
    super(scene, array);
    const backgroundTexture = 'window_stock';
    const background = scene.add.sprite(config.width / 2, config.height / 2, backgroundTexture);
    const backgroundScale = 0.25;
    background.setScale(backgroundScale);
    background.setOrigin(0.5, 0.5);
    this.background = background;
    this.add(background);

    const closeTexture = 'button_close';
    const paddingX = 10;
    const paddingY = 5;
    const x = background.x + (background.width * backgroundScale) / 2 - paddingX;
    const y = background.y - (background.height * backgroundScale) / 2 + paddingY;
    const close = scene.add.sprite(x, y, closeTexture);
    close.setOrigin(0.5, 0.5);
    this.add(close);

    this.scene.input.on('pointerdown', () => {
      this.hideWindow();
    });

    // this.hide();
  }
}
