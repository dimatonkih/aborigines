import config from '../../game.json';
import CustomGroup from '../Phaser Custom/CustomGroup';

export default class UIStatic extends CustomGroup {
  constructor(scene, array) {
    super(scene, array);
    // todo rename buttons to minimenu;
    const miniMenuBackground = this.create(config.width, 0, 'buttons');
    miniMenuBackground.setOrigin(1, 0);

    const settingsIcon = this.create(config.width - 6, 4, 'icon_settings');
    settingsIcon.setOrigin(1, 0);
  }
}
