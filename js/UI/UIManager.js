import UIStorage from './UIStorage';
import UIStatic from './UIStatic';

export default class UIManager {
  constructor(scene, array) {
    this.static = new UIStatic(scene, array);
    this.storage = new UIStorage(scene, array);
  }
}
