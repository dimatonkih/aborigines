import CustomGroup from '../Phaser Custom/CustomGroup';

export default class UIWindow extends CustomGroup {
  constructor(scene, array) {
    super(scene, array);
    this.eventContext = 'UI';
  }

  hideWindow() {
    this.hide();
    this.unregisterEvents();
  }

  showWindow() {
    this.show();
    this.registerEvents();
  }

  onEscClicked() {
    this.hideWindow();
  }

  registerEvents() {
    this.scene.input.keyboard.on('keydown', (keyboard) => {
      if (keyboard.key === 'Escape') {
        this.onEscClicked();
      }
    }, this.eventContext);
  }

  unregisterEvents() {
    this.scene.input.keyboard.off(null, null, this.eventContext);
  }
}
