import Phaser from 'phaser';
import CustomGroup from './Phaser Custom/CustomGroup';


export default class InteractableObject extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, texture) {
    super(scene, x, y, texture);
    scene.add.existing(this);
    this.actions = [];
    this.setInteractive();
    this.actionIconsGroup = new CustomGroup(scene);
    // this.initActionGroup(scene);
  }

  initActionGroup() {
    let offset = 50;
    const list = Object.entries(this.actions);
    this.getActions();
    list.forEach(([actionName, action]) => {
      // todo move to actions
      const iconTexture = `get${actionName}`;
      const y = this.height;
      const x = offset + 50;
      const actionIcon = this.actionIconsGroup.create(x, y, iconTexture);
      offset += 50;
      actionIcon.setInteractive();
      actionIcon.on('pointerdown', (pointer) => {
        const { target } = pointer;

        if (!target) {
          console.warn('No target');
          return;
        }

        const actionWrapper = () => {
          this.occupied = true;
          action(this, (item) => {
            console.log(item);
            this.occupied = false;
          });
        };
        this.actionIconsGroup.hide();
        this.onClick(target, actionWrapper);
      });
    });
    this.actionIconsGroup.hide();

    this.on('pointerdown', this.onActionGroupClicked);
  }

  getActions() {
    console.log(this.actions);
  }

  onClick(human, action) {
    human.setAction(action);
    human.setTarget(this);
    human.move(this.x);
  }

  onActionGroupClicked() {
    if (this.occupied) {
      console.warn('Object is occupied');
      return;
    }

    this.actionIconsGroup.toggle();
  }
}
