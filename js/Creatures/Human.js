import Phaser from 'phaser';

export default class Human extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, gender) {
    super(scene, x, y, gender);
    this.setOrigin(0.5, 1);
    this.speed = 40;
    scene.add.existing(this);
    scene.physics.add.existing(this);
    // this.job = "woodman"
    this.toggleFlipX();
    this.rotated = 'right';
    this.positionToMove = null;

    this.setInteractive();
    this.on('pointerdown', (pointer, x, y) => {
      pointer.target = this;
      console.log(pointer.target);
    });
  }

  setAction(action) {
    this.action = action;
  }

  setTarget(target) {
    this.target = target;
  }

  moveRight(x) {
    if (this.rotated === 'left' && this.x < x) {
      this.toggleFlipX();
      this.rotated = 'right';
    }
    this.body.velocity.x = this.speed;
  }

  moveLeft(x) {
    if (this.rotated === 'right' && this.x > x) {
      this.toggleFlipX();
      this.rotated = 'left';
    }
    this.body.velocity.x = -this.speed;
  }

  move(x) {
    this.positionToMove = x;
    if (this.x > x) this.moveLeft(x);
    if (this.x < x) this.moveRight(x);
  }

  // tood checkout http://labs.phaser.io/edit.html?src=src/physics\arcade\move%20to%20pointer.js and make move to
  preUpdate() {
    if (this.positionToMove) {
      const stopMovingLeft = this.rotated === 'left' && this.x <= this.positionToMove;
      const stopMovingRight = this.rotated === 'right' && this.x >= this.positionToMove;
      if (stopMovingLeft || stopMovingRight) {
        this.body.velocity.x = 0;
        this.positionToMove = null;
        if (this.action) {
          this.action();
        } else {
          throw new Error('There\'s no action to execute');
        }
      }
    }
  }
}
