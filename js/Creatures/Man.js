import Human from './Human';

export default class Man extends Human {
  constructor(scene, x, y) {
    super(scene, x, y, 'man');
  }
}
