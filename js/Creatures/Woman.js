import Human from './Human';

export default class Woman extends Human {
  constructor(scene, x, y) {
    super(scene, x, y, 'woman');
  }
}
