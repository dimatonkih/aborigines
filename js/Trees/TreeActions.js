import Item from '../Items/Item';

export function getDefaultAction(actionName = 'Default') {
  return function (interactableObject, cb) {
    const { scene, x, y } = interactableObject;
    const texture = 'nonpass';
    const item = new Item(scene, x, y, texture);
    console.log(`${actionName} action`, interactableObject);
    if (cb) {
      return cb(item);
    }
    return item;
  };
}

export function getPourAction() {
  return function (interactableObject, cb) {
    console.log('Pouring', interactableObject);
    if (cb) {
      return cb();
    }
    return;
  };
}

export function getWoodAction() {
  return function (interactableObject, cb) {
    const cd = 10 * 1000;
    this.alpha = 0;
    setTimeout(() => {
      this.alpha = 1;
    }, cd);
    const { scene, x, y } = interactableObject;
    const texture = 'item_wood';
    const item = new Item(scene, x, y, texture);
    console.log('Cutting item', interactableObject);
    if (cb) {
      return cb(item);
    }
    return item;
  };
}
