import InteractableObject from '../InteractableObject';
import { getDefaultAction } from './TreeActions';

export default class CocoPalm extends InteractableObject {
  constructor(scene, x, y) {
    const texture = 'cocopalm';
    super(scene, x, y, texture);
    this.setOrigin(0.5, 1);
    this.actions = {
      palmleaves: getDefaultAction('Palmleaves'),
      wood: getDefaultAction('Wood'),
      coconuts: getDefaultAction('Coconuts'),
    };
    this.initActionGroup();
  }
}
