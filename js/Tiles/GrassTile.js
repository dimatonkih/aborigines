import Tile from './Tile';

export default class GrassTile extends Tile {
  constructor(scene, x, y) {
    const texture = 'grass0';
    super(scene, x, y, texture);
  }
}
