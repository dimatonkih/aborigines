import Phaser from 'phaser';
import config from '../../game.json';

export default class Tile extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, texture) {
    super(scene, x, y, texture);
    this.setOrigin(0, 0);
    this.width = config.tile.width;
    this.height = config.tile.height;
    scene.add.existing(this);
  }
}
