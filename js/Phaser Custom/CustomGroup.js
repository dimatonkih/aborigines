import Phaser from 'phaser';

export default class CustomGroup extends Phaser.GameObjects.Group {
  constructor(scene, array) {
    super(scene, array);
    this.enable = true;
  }

  hide() {
    this.each((entry) => entry.alpha = 0);
    this.enable = false;
  }

  show() {
    this.each((entry) => entry.alpha = 1);
    this.enable = true;
  }

  toggle(bool) {
    if (!this.enable || bool) {
      this.show();
    } else if (this.enable || !bool) {
      this.hide();
    }
  }

  each(cb) {
    this.children.entries.forEach(cb);
  }

  getEntries() {
    return this.children.entries;
  }
}
