import Phaser from 'phaser';
import MenuScene from './Scenes/MenuScene';
import GameScene from './Scenes/GameScene';
import config from '../game.json';

const gameConfig = {
  type: Phaser.AUTO,
  width: config.width,
  height: config.height,
  zoom: 4,
  title: 'The Tribez',
  scene: [MenuScene, GameScene],
  physics: {
    default: 'arcade',
  },
};

const gameInstance = new Phaser.Game(gameConfig);
