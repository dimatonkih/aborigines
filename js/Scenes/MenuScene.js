import Phaser from 'phaser';
import resources from '../../resources.json';
import config from '../../game.json';

export default class MenuScene extends Phaser.Scene {
  constructor() {
    super({ key: 'MenuScene' });
  }

  preload() {
    const padding = 10;
    const progressBarHeight = 30;
    const progressBarWidth = (config.width - padding * 2);
    const progressBarColorBackground = 0x222222;
    const progressBarColor = 0x2dd861;
    const progressBarPosition = {
      x: padding,
      y: config.height - progressBarHeight - padding,
    };

    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(progressBarColorBackground, 0.9);
    progressBox.fillRect(progressBarPosition.x, progressBarPosition.y, progressBarWidth, progressBarHeight);

    const { width } = this.cameras.main;
    const { height } = this.cameras.main;
    const loadingText = this.make.text({
      x: width / 2,
      y: height / 2 - 50,
      text: 'Loading...',
      style: {
        font: '12px monospace',
        fill: '#ffffff',
      },
    });
    loadingText.setOrigin(0.5, 0.5);

    const percentText = this.make.text({
      x: width / 2,
      y: height / 2 - 5,
      text: '0%',
      style: {
        font: '12px monospace',
        fill: '#ffffff',
      },
    });
    percentText.setOrigin(0.5, 0.5);

    const assetText = this.make.text({
      x: width / 2,
      y: height / 2 + 50,
      text: '',
      style: {
        font: '12px monospace',
        fill: '#ffffff',
      },
    });
    assetText.setOrigin(0.5, 0.5);

    this.load.on('progress', (value) => {
      percentText.setText(`${parseInt(value * 100)}%`);
      progressBar.clear();
      progressBar.fillStyle(progressBarColor, 1);
      progressBar.fillRect(progressBarPosition.x, progressBarPosition.y, progressBarWidth * value, progressBarHeight);
    });

    this.load.on('fileprogress', (file) => {
      assetText.setText(`Loading asset: ${file.key}`);
    });

    this.load.on('complete', () => {
      progressBar.destroy();
      progressBox.destroy();
      loadingText.destroy();
      percentText.destroy();
      assetText.destroy();

      const newGameBtn = new Button(this, { text: 'new game', x: config.width / 2, y: config.height / 2 });
      newGameBtn.action = () => this.scene.start('GameScene');

      const settingsBtn = new Button(this, { text: 'settings', x: config.width / 2, y: config.height / 2 + 50 });
      settingsBtn.action = () => console.log('implement opening/closing settings');
    });

    Object.keys(resources).forEach((key) => {
      const value = resources[key];
      this.load.image(key, value);
    });
  }
}

function Button(scene, settings) {
  const { text, x, y } = settings;
  const button = scene.make.text({
    x,
    y,
    text,
    style: {
      font: '12px monospace',
      fill: '#ffffff',
    },
  });
  button.setInteractive({ useHandCursor: true });
  button.setOrigin(0.5, 0.5);
  button.on('pointerover', (pointer) => {
    button.setFill('#333333');
  });

  button.on('pointerout', (pointer) => {
    button.setFill('#ffffff');
  });

  button.on('pointerdown', () => {
    if (button.action) {
      button.action(button);
    }
  });

  return button;
}
