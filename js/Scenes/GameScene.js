import Phaser from 'phaser';

import World from '../World';
import config from '../../game.json';
import CocoPalm from '../Trees/CocoPalm';
import Rock from '../Items/Rock';
import Woman from '../Creatures/Woman';
import Man from '../Creatures/Man';
import Building from '../Building';

export default class GameScene extends Phaser.Scene {
  constructor() {
    super({ key: 'GameScene' });
  }

  create() {
    const scene = this;
    const world = new World(scene);
    const height = config.height - config.tile.height;
    const { width } = config;

    const cocoPalm = new CocoPalm(scene, width / 2, height);
    const rock = new Rock(scene, width / 3, height);
    world.objects.push(cocoPalm, rock);
    const woman = new Woman(scene, 40, height);
    const man = new Man(scene, 40, height);
    world.humans.push(woman, man);
    world.buildings.push(new Building(scene));

    this.world = world;
  }

  update(time, delta) {
    this.world.sun.update(time, delta);
  }
}
