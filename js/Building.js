import Phaser from 'phaser';
import config from '../game.json';

export default class Building extends Phaser.GameObjects.Sprite {
  constructor(scene, texture) {
    const y = config.height - config.tile.height;
    super(scene, 0, y, texture);
    scene.add.existing(this);
    this.alpha = 0.4;
    this.setOrigin(0.5, 1);
    // this.registerEvents();
    this.eventsContext = 'building';
  }

  registerEvents() {
    const { input } = this.scene;
    console.log('Building events registered');
    input.on('pointermove', (pointer) => {
      this.onMouseMoved(pointer);
    }, this.eventsContext);
    input.on('pointerdown', (pointer) => {
      this.onMouseClicked(pointer);
    }, this.eventsContext);
    input.keyboard.on('keydown', (keyboard) => {
      if (keyboard.key === 'Escape') {
        this.onEscClicked();
      }
    }, this.eventsContext);
  }

  unRegisterEvents() {
    console.log('Building events unregistered');
    // todo check if it will throws some error or some shit
    this.scene.input.removeListener('pointermove', null, this.eventsContext);
    this.scene.input.removeListener('pointerdown', null, this.eventsContext);
    this.scene.input.keyboard.removeListener('keydown', null, this.eventsContext);
  }

  build(x) {
    this.alpha = 1;
    this.x = x;
    this.unRegisterEvents();
  }

  onMouseMoved(pointer) {
    // todo set grid;
    this.updatePosition(pointer);
  }

  onMouseClicked(pointer) {
    this.build(pointer.x);
  }

  onEscClicked() {
    this.unRegisterEvents();
  }

  updatePosition(pointer) {
    this.x = pointer.x;
  }
}
