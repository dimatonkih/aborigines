import Phaser from 'phaser';

export default class Item extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, texture) {
    super(scene, x, y, texture);
    this.setOrigin(0.5, 1);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.setInteractive();
    console.log(this);
    this.on('pointerdown', () => {
      this.scene.game.storeManager.setItem(this);
      this.destroy();
    });
  }
}
