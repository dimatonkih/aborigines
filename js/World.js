import GrassTile from './Tiles/GrassTile';
import config from '../game.json';
import Sun from './Sun';

export default class World {
  constructor(scene) {
    this.tiles = [];
    this.items = [];
    this.humans = [];
    this.buildings = [];
    this.background = [];
    this.objects = [];
    this.setBackground(scene);
    this.setGrassLayer(scene);
  }

  setTile(tile) {
    this.tiles.push(tile);
  }

  setGrassLayer(scene) {
    let tilePositionX = 0;
    let tilesCount = 16;
    const height = config.height - config.tile.height;
    while (tilesCount) {
      const tile = new GrassTile(scene, tilePositionX, height);
      this.setTile(tile);
      tilePositionX += tile.width;
      tilesCount -= 1;
    }
  }

  setBackground(scene) {
    const backgroundDay = scene.add.image(0, 0, 'skyday');
    backgroundDay.setOrigin(0, 0);
    const height = config.height - config.tile.height;

    this.sun = new Sun(scene);

    const jungleDay = scene.add.image(0, height, 'jungleday');
    jungleDay.setOrigin(0, 1);

    // const moon = new Moon();
    this.background.push(backgroundDay);
  }
}
