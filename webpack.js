import path from 'path';
import webpack from 'webpack';
import { assetsToJson } from './utils/assetsToJson';

const config = {
  target: 'web',
  mode: 'development',
  entry: {
    main: [
      './index.js',
      //   'webpack/hot/dev-server',
      //   'webpack-hot-middleware/client'
    ],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              ['@babel/plugin-transform-runtime'],
            ],
          },
        },
      },
    ],
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  output: {
    filename: './game.js',
    // path: path.resolve(__dirname, './js')
  },
  context: path.resolve(__dirname, './js'),
};

function scripts() {
  assetsToJson();
  return new Promise((resolve) => webpack(config, (err, stats) => {
    if (err) console.log('Webpack', err);

    console.log(stats.toString({ /* stats options */ }));

    resolve();
  }));
}

export { config, scripts };
