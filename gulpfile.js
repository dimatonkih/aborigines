import gulp from 'gulp';
import Browser from 'browser-sync';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import { scripts, config as webpackConfig } from './webpack';
import { assetsToJson } from './utils/assetsToJson';

const browser = Browser.create();
const bundler = webpack(webpackConfig);

export function server() {
  const config = {
    server: '.',
    middleware: [
      webpackDevMiddleware(bundler, { /* options */ }),
      webpackHotMiddleware(bundler),
    ],
  };
  browser.init(config);
  gulp.watch('js/**/*.js').on('change', () => browser.reload());
}

export const dev = gulp.series(server);
export const build = gulp.series(scripts);

export default dev;
